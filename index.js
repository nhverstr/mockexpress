import express from 'express';
const app = express();

app.get('/test', (req, res) => {
	res.json({
		message: 'Hello, this is a mocked API endpoint.',
		request: {
			method: req.method,
			headers: req.headers,
			body: req.body
		}
	});
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
	console.log(`Mock server listening on port ${port}`);
});